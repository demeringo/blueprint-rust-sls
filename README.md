# blueprint-rust-sls

A basic demo of using the Serverless framework ⚡ to deploy a Rust functions 🦀 as AWS lambda.

## Usage

We need to cross compile for lambda runtime (x86_64-unknown-linux-musl).

You need to install musl for local cross compilation

```sh
sudo apt install musl-tools

# Install
rustup target add x86_64-unknown-linux-musl
```

## Create distinct handlers (binaries) for each function

See example in Cargo.toml

```toml
[[bin]]
name = "hello"
path = "src/hello.rs"

[[bin]]
name = "bye"
path = "src/bye.rs"
```

The 2 generated binaries are part of the same package name  (demo_app in our case) so they need to be declared as this in Serverless.yml

```yml
functions:
  hello:
    # handler value syntax is `{cargo-package-name}.{bin-name}`
    # or `{cargo-package-name}` for short when you are building a
    # default bin for a given package.
    handler: "demo_app.hello"
    events:
      - http:
          path: /hello
          method: GET
  
  bye:
    # handler value syntax is `{cargo-package-name}.{bin-name}`
    # or `{cargo-package-name}` for short when you are building a
    # default bin for a given package.
    handler: "demo_app.bye"
    events:
      - http:
          path: /bye
          method: GET 
```

## Thanks

This project is inspired by this great article: [Rust with Serverless Framework: Introduction](https://scicoding.com/serverless-rust-introduction/)
