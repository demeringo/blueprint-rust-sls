use lambda_http::{http::StatusCode, IntoResponse, Request, Response};
use lambda_runtime::Error;
use serde_json::json;

#[tokio::main]
async fn main() -> Result<(), Error> {
    lambda_http::run(lambda_http::service_fn(hello)).await?;
    Ok(())
}

async fn hello(event: Request) -> Result<impl IntoResponse, Error> {
    println!("Hello invoked with event : {:?}", event);
    Ok(response(
        StatusCode::OK,
        json!({"message": "Hello function was called"}).to_string(),
    ))
}

/// HTTP Response with a JSON payload
fn response(status_code: StatusCode, body: String) -> Response<String> {
    Response::builder()
        .status(status_code)
        .header("Content-Type", "application/json")
        .body(body)
        .unwrap()
}
