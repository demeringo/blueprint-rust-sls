use lambda_http::{http::StatusCode, IntoResponse, Request, Response};
use lambda_runtime::Error;
use lambda_runtime::LambdaEvent;
use serde_json::{json, Value};

#[tokio::main]
async fn main() -> Result<(), Error> {
    lambda_http::run(lambda_http::service_fn(hello)).await?;
    lambda_http::run(lambda_http::service_fn(bye)).await?;
    Ok(())
}

async fn hello(event: Request) -> Result<impl IntoResponse, Error> {
    println!("Hello invoked with event : {:?}", event);
    Ok(response(
        StatusCode::OK,
        json!({"message": "Hello function was called"}).to_string(),
    ))
}

async fn bye(event: Request) -> Result<impl IntoResponse, Error> {
    println!("Bye invoked with event : {:?}", event);
    Ok(response(
        StatusCode::OK,
        json!({"message": "Bye function was called!"}).to_string(),
    ))
}

// Using a lambda event
#[allow(dead_code)]
async fn func(event: LambdaEvent<Value>) -> Result<Value, Error> {
    let (event, _context) = event.into_parts();
    let first_name = event["firstName"].as_str().unwrap_or("world");

    Ok(json!({ "message": format!("Hello, {}!", first_name) }))
}

/// HTTP Response with a JSON payload
fn response(status_code: StatusCode, body: String) -> Response<String> {
    Response::builder()
        .status(status_code)
        .header("Content-Type", "application/json")
        .body(body)
        .unwrap()
}

#[cfg(test)]
mod tests {
    use super::*;
    use lambda_runtime::Context;
    use lambda_runtime::LambdaEvent;

    #[tokio::test]
    async fn hello_handles() {
        let request = Request::default();
        let expected = json!({
            "message": "Hello function was called"
        })
        .into_response();
        let response = hello(request)
            .await
            .expect("expected Ok(_) value")
            .into_response();
        assert_eq!(response.body(), expected.body())
    }

    #[tokio::test]
    async fn handler_default_parameter() {
        let event = LambdaEvent::new(json!({}), Context::default());
        assert_eq!(
            func(event.clone()).await.expect("expected Ok(_) value"),
            json!({"message": "Hello, world!"})
        )
    }

    #[tokio::test]
    async fn handler_custom_parameter() {
        let event = LambdaEvent::new(json!({"firstName": "Jake"}), Context::default());
        assert_eq!(
            func(event.clone()).await.expect("expected Ok(_) value"),
            json!({"message": "Hello, Jake!"})
        )
    }
}
